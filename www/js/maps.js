var map;
var origin;
var geocoder = new google.maps.Geocoder;
var currentLng;
var currentLat;

var lastLocation,
    currentLocationMarker, locationAccuracyMarker, polyline,
    startMarker, endMarker,
    markers = [];

function initMap(){
  map = new google.maps.Map(document.getElementById('map'), {
    zoomControl: false,
    mapTypeControl: false,
    streetViewControl: false,
    center: new google.maps.LatLng(1.3521, 103.8198),
    zoom: 15
  });
}

function showMap(){
  if($$('#map').css('z-index') == 0){
    $$('#map').css('z-index', 1);
    $$('#map-hide').show();
  } else {
    $$('#map').css('z-index', 0);
    $$('#map-hide').hide();
  }
}

function setCurrentLocationMarker(location) {
  if (!currentLocationMarker) {
    currentLocationMarker = new google.maps.Marker({
      map: map,
      zIndex: 10,
      title: html10n.get('current-location'),
      icon: {
        path: google.maps.SymbolPath.CIRCLE,
        scale: 12,
        fillColor: '#2677FF',
        fillOpacity: 1,
        strokeColor: '#ffffff',
        strokeOpacity: 1,
        strokeWeight: 6
      }
    });
    locationAccuracyMarker = new google.maps.Circle({
      zIndex: 9,
      fillColor: '#3366cc',
      fillOpacity: 0.4,
      strokeOpacity: 0,
      map: map
    });
  }

  var latlng = new google.maps.LatLng(location.latitude, location.longitude);

  currentLocationMarker.setPosition(latlng);
  locationAccuracyMarker.setCenter(latlng);
  locationAccuracyMarker.setRadius(location.accuracy);
}

function setLocationPolyline(location) {
  if (!polyline) {
    polyline = new google.maps.Polyline({
      zIndex: 1,
      map: map,
      geodesic: true,
      strokeColor: '#2677FF',
      strokeOpacity: 0.7,
      strokeWeight: 5
    });
  }

  if (lastLocation) {
    if (location !=  lastLocation) {
      markers.push(new google.maps.Marker({
        zIndex: 1,
        icon: {
          path: google.maps.SymbolPath.CIRCLE,
          scale: 7,
          fillColor: '#11b700',//'26cc77',
          fillOpacity: 1,
          strokeColor: '#0d6104',
          strokeWeight: 1,
          strokeOpacity: 0.7
        },
        map: map,
        position: new google.maps.LatLng(lastLocation.latitude, lastLocation.longitude)
      }));

      var journey = JSON.parse(localStorage.getItem('journey'));
      if(journey === null) journey = [];

      journey.push({id: journeyId, lat: location.latitude, lng: location.longitude});
      localStorage.setItem('journey', JSON.stringify(journey));
    }

    var latlng = new google.maps.LatLng(location.latitude, location.longitude);

    polyline.getPath().push(latlng);
  }

  lastLocation = location;
}

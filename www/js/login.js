function login(data){
  var xhttp = new XMLHttpRequest();
  var value = "email="+ data.email;
  xhttp.onreadystatechange=function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var response = JSON.parse(xhttp.response);
      localStorage.setItem("authenticated", true);
      localStorage.setItem("id", response.user.id);
      localStorage.setItem("name", response.user.name);
      localStorage.setItem("email", data.email);
      localStorage.setItem("password", data.password);
      app.hidePreloader();
      mainView.router.load({pageName: 'index'});
      initMap();
      initGeolocation();
      initVoiceRecognition();
      sync();
      speak(html10n.get('welcome'));
    }
    else if (xhttp.readyState == 4 && xhttp.status == 401) {
      app.hidePreloader();
      app.alert(html10n.get("wrong-email"), html10n.get("login-failed"));
    }
  };
  xhttp.open("POST", SERVER_URL +"/v1/api/authenticate", true);
  xhttp.setRequestHeader("Authorization", "Basic "+ btoa(data.email +":"+ data.password));
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(value);
  app.showPreloader(html10n.get('logging-in'));
}

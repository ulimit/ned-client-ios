var checkedFixedDistance = [];
var checkedSteps = [];
var checkedSubSteps = [];
var checkedDirection= [];
var stops = [];
document.getElementById('steps-content').innerHTML = '';

var geofenceIntervalId, uploadLogsIntervalId;

function journey(id){
  checkedFixedDistance = [];
  checkedSteps = [];
  checkedSubSteps = [];
  checkedDirection= [];
  stops = [];
  document.getElementById('steps-content').innerHTML = '';

  //Journey logs uploader
  uploadLogsIntervalId = setInterval(
    function(){
      uploadLogs();
    },
    600000
  );

  //Geofence checker
  geofenceIntervalId = setInterval(
    function(){
      var routes = JSON.parse(localStorage.getItem('routes'));
      var contacts = JSON.parse(localStorage.getItem('contacts'));

      if (google.maps.geometry.poly.isLocationOnEdge(origin,
        new google.maps.Polyline({path: routes[id].overview_path}),
        0.005))
      {
        return;
      }
      else {
        speak(html10n.get('alert-off-course'));

        app.closeModal();
        app.alert(html10n.get('alert-off-course'), 'NED');

        sendSMS(
          localStorage.getItem('name'),
          contacts.contact_number,
          html10n.get('off-course-sms-msg', { contact: contacts.contact, name: localStorage.getItem('name')}));
      }
    },
    300000
  );
}

function navigate(id){
  var routes = JSON.parse(localStorage.getItem('routes'));
  var watchRoute = routes[id];

  for (var i = 0; i < watchRoute.legs.length; i++) {
    var watchRouteLeg = watchRoute.legs[i];

    for (var x = 0; x < watchRouteLeg.steps.length; x++) {
      var watchRouteLegSteps = watchRouteLeg.steps;
      var watchRouteLegStep = watchRouteLeg.steps[x];
      var stepDistance = google.maps.geometry.spherical.computeDistanceBetween(
        new google.maps.LatLng(watchRouteLegStep.start_location),
        new google.maps.LatLng(watchRouteLegStep.end_location)
      );
      var stepStartDistance = google.maps.geometry.spherical.computeDistanceBetween(origin, new google.maps.LatLng(watchRouteLegStep.start_location));
      var stepEndDistance = google.maps.geometry.spherical.computeDistanceBetween(origin, new google.maps.LatLng(watchRouteLegStep.end_location));

      if (typeof checkedSteps[x] == "undefined") {
        checkedSteps.push({checked: false});
        stops.push('empty');
      }

      if (typeof checkedFixedDistance[x] == "undefined") {
        checkedFixedDistance.push({checked: null});
      }

      //Alert next waypoint in 300m
      if (stepEndDistance <= 300 && (stepDistance/2) >= 300 && checkedFixedDistance[x].checked == null) {
        var nextStop = '';
        if (typeof watchRouteLegStep.steps == "undefined"){
          nextStop = watchRouteLegStep.transit.arrival_stop.name;
        }else{
          nextStop = watchRouteLegStep.instructions.replace('Walk to','');
        }
        speak(html10n.get('alert-distance', { stop: nextStop, distance: stepEndDistance.toFixed(2)}));
        app.closeModal();
        app.alert(html10n.get('alert-distance', { stop: nextStop, distance: stepEndDistance.toFixed(2)}), 'NED');
        checkedFixedDistance[x].checked = false;
      }

      //Alert next waypoint in 100m
      if (stepEndDistance <= 100 && (stepDistance/2) >= 100 && (checkedFixedDistance[x].checked == null || checkedFixedDistance[x].checked == false )) {
        var nextStop = '';
        if (typeof watchRouteLegStep.steps == "undefined"){
          nextStop = watchRouteLegStep.transit.arrival_stop.name;
        }else{
          nextStop = watchRouteLegStep.instructions.replace('Walk to','');
        }
        speak(html10n.get('alert-distance', { stop: nextStop, distance: stepEndDistance.toFixed(2)}));
        app.closeModal();
        app.alert(html10n.get('alert-distance', { stop: nextStop, distance: stepEndDistance.toFixed(2)}), 'NED');
        checkedFixedDistance[x].checked = true;
      }

      //Alert next waypoint in quarter distance
      if (stepEndDistance <= (stepDistance/4) && checkedSteps[x].checked == false) {
        var nextStop = '';
        if (typeof watchRouteLegStep.steps == "undefined"){
          nextStop = watchRouteLegStep.transit.arrival_stop.name;
        }else{
          nextStop = watchRouteLegStep.instructions.replace('Walk to','');
        }
        speak(html10n.get('alert-distance', { stop: nextStop, distance: stepEndDistance.toFixed(2)}));
        app.closeModal();
        app.alert(html10n.get('alert-distance', { stop: nextStop, distance: stepEndDistance.toFixed(2)}), 'NED');
        checkedSteps[x].checked = true;
      }
    }
  }

  for (var i = 0; i < watchRoute.legs.length; i++) {
    var watchRouteLeg = watchRoute.legs[i];

    for (var x = 0; x < watchRouteLeg.steps.length; x++) {
      var watchRouteLegStep = watchRouteLeg.steps[x];
      var stepDistance = google.maps.geometry.spherical.computeDistanceBetween(
        new google.maps.LatLng(watchRouteLegStep.start_location),
        new google.maps.LatLng(watchRouteLegStep.end_location)
      );
      var stepStartDistance = google.maps.geometry.spherical.computeDistanceBetween(origin, new google.maps.LatLng(watchRouteLegStep.start_location));
      var stepEndDistance = google.maps.geometry.spherical.computeDistanceBetween(origin, new google.maps.LatLng(watchRouteLegStep.end_location));

      if (typeof watchRouteLegStep.steps == "undefined") {
        var details = getVehicleInfo(watchRouteLeg.steps, x);
        //Check current step
        if (stepStartDistance <= (subSteptepDistance/4) && checkedSteps[x].checked == false) {
          $$('#step-'+ x).addClass('selected');
          speak(watchRouteLegStep.instructions.replace(/<\/?[^>]+(>|$)/g, ""));
        }

        //Check bus stop
        if (details.type == "BUS" && checkedSteps[x].checked == false){
          for (var i = 0; i < stops[x].length; i++) {
            var stop = stops[x][i];
            var busStopDistance = google.maps.geometry.spherical.computeDistanceBetween(origin, new google.maps.LatLng({
              lat: Number(stop.bus_stop.latitude),
              lng: Number(stop.bus_stop.longitude)
            }));

            if (busStopDistance <= 20) {
              $$('#step-'+ x +'busstop-'+ i).addClass('selected');
              $$('#step-'+ x +'busstop-'+ (i -1)).addClass('deselected');
            }
          }
        }

        //Check completed step
        if (stepEndDistance <= (stepDistance/4) && checkedSteps[x].checked == true) {
          $$('#step-'+ x).addClass('deselected');
        }
      }
      else {
        for (var y = 0; y < watchRouteLegStep.steps.length; y++) {
          var watchRouteLegSubStep = watchRouteLegStep.steps[y];
          var subSteptepDistance = google.maps.geometry.spherical.computeDistanceBetween(
            new google.maps.LatLng(watchRouteLegSubStep.start_location),
            new google.maps.LatLng(watchRouteLegSubStep.end_location)
          );
          var subStepStartDistance = google.maps.geometry.spherical.computeDistanceBetween(origin, new google.maps.LatLng(watchRouteLegSubStep.start_location));
          var subStepEndDistance = google.maps.geometry.spherical.computeDistanceBetween(origin, new google.maps.LatLng(watchRouteLegSubStep.end_location));

          if (typeof checkedSubSteps[y] == "undefined") {
            checkedSubSteps.push({checked: null});
          }

          if (typeof checkedDirection[y] == "undefined") {
            checkedDirection.push({checked: null});
          }
          //Check direction
          if (subStepStartDistance < (subSteptepDistance/10) && checkedDirection[y].checked == null) {
            checkedDirection[y].checked = false;
          }
          //Check wrong direction
          if ((subStepStartDistance + subStepEndDistance) > (subSteptepDistance + (subSteptepDistance/5)) && checkedDirection[y].checked == false) {
            //Wrong direction alert
            speak(html10n.get('wrong-direction'));
            app.alert(html10n.get('wrong-direction'), 'NED');
            app.modal({
              title: 'NED',
              text: html10n.get('wrong-direction-re-route'),
              buttons: [
                {
                  text: html10n.get('cancel'),
                  onClick: function() {
                    app.closeModal();
                  }
                },
                {
                  text: html10n.get('re-route'),
                  onClick: function() {
                    app.modal({
                      title: html10n.get('are-you-sure'),
                      text: html10n.get('proceed-and-re-route'),
                      buttons: [
                        {
                          text: html10n.get('cancel'),
                          onClick: function() {
                            app.closeModal();
                          }
                        },
                        {
                          text: html10n.get('proceed'),
                          onClick: function() {
                            backgroundGeolocation.stop();
                            syncHistory();
                            isJourneyActive = false;
                            clearInterval(geofenceIntervalId);
                            document.getElementById('route-list').innerHTML = '';
                            mainView.router.load({pageName: 'go'});
                            directionsDisplay.setDirections({routes: []});
                            journeyId = null;
                            uploadLogs();
                            origin_id = null;
                            initGeolocationThenGo();
                            app.closeModal();
                          }
                        }
                      ]
                    });
                  }
                }
              ]
            });
            checkedDirection[y].checked = true;
          }
          //Check current substep
          if (subStepStartDistance <= (subSteptepDistance/4) && checkedSubSteps[y].checked == null) {
            $$('#step-'+ x +'-substep-'+ y).addClass('selected');
            checkedSubSteps[y].checked = false;
            speak(watchRouteLegSubStep.instructions.replace(/<\/?[^>]+(>|$)/g, ""));
          }
          //Check complete substep
          if (subStepEndDistance <= (subSteptepDistance/4) && checkedSubSteps[y].checked == false) {
            $$('#step-'+ x +'-substep-'+ y).removeClass('selected');
            $$('#step-'+ x +'-substep-'+ y).addClass('deselected');
            checkedSubSteps[y].checked = true;
            checkedDirection[y].checked = true;
          }
        }
      }
    }
  }
}

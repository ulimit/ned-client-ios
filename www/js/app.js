document.addEventListener("deviceready", onDeviceReady, false);

var app = new Framework7();

var $$ = Dom7;

var mainView = app.addView('.view-main', {
  domCache: true,
  dynamicNavbar: true
});

$$(document).on('pageInit', function (e) {
  $$('#map').css('z-index', 0);
  $$('#map-hide').hide();
})

$$(document).on('pageReinit', function (e) {
  $$('#map').css('z-index', 0);
  $$('#map-hide').hide();
})

app.onPageInit('go', function (page) {
  initGo();
  $$(document).on('DOMNodeInserted', '.pac-container', function() {
    $$('.pac-item, .pac-item span', this).addClass('no-fastclick');
  });
  $$('body').on('touchend','.pac-container', function(e){
    e.stopImmediatePropagation();
  });
});

app.onPageInit('nearby', function (page) {
  $$('body').on('click','#nearby-button', function(){
    var formData = app.formToJSON('#nearby-form');
    nearby(formData);
  });

  $$('body').on('click','#nearby-loc', function(){
    $$("#nearby-event").removeClass('active');
    $$("#nearby-location").addClass('active');

    $$("#nearby-eve").removeClass('active');
    $$("#nearby-loc").addClass('active');
  });

  $$('body').on('click','#nearby-eve', function(){
    $$("#nearby-location").removeClass('active');
    $$("#nearby-event").addClass('active');

    $$("#nearby-loc").removeClass('active');
    $$("#nearby-eve").addClass('active');
    app.showIndicator();
    nearbyEvent();
  });
});

app.onPageInit('contacts', function (page) {
  var contacts = JSON.parse(localStorage.getItem('contacts'));

  $$('body').on('click','#main-contact-edit', function(){
    contacts = JSON.parse(localStorage.getItem('contacts'));
    editContact('main',contacts.contact, contacts.contact_number);
  });
  $$('body').on('click','#alternate-contact-edit', function(){
    contacts = JSON.parse(localStorage.getItem('contacts'));
    editContact('alternate',contacts.alternate, contacts.alternate_number);
  });

  $$('body').on('click','#contact-save-button', function(){
    var formData = app.formToJSON('#contact-form');
    saveContact(formData);
  });

  $$('body').on('click','#main-contact-call', function(){
    app.modal({
      title: contacts.contact_number,
      text: html10n.get('call-this-number'),
      buttons: [
        {
          text: html10n.get('cancel'),
          onClick: function() {
            app.closeModal();
          }
        },
        {
          text: html10n.get('call'),
          onClick: function() {

            sendSMS(
              localStorage.getItem('name'),
              contacts.contact_number,
              html10n.get('call-sms-msg', { contact: contacts.alternate, name: localStorage.getItem('name')}));

            cordova.InAppBrowser.open('tel:'+ contacts.contact_number, '_system');
          }
        }
      ]
    });
  });
  $$('body').on('click','#alternate-contact-call', function(){
    app.modal({
      title: contacts.alternate_number,
      text: html10n.get('call-this-number'),
      buttons: [
        {
          text: html10n.get('cancel'),
          onClick: function() {
            app.closeModal();
          }
        },
        {
          text: html10n.get('call'),
          onClick: function() {

            sendSMS(
              localStorage.getItem('name'),
              contacts.alternate_number,
              html10n.get('call-sms-msg', { contact: contacts.alternate, name: localStorage.getItem('name')}));

            cordova.InAppBrowser.open('tel:'+ contacts.alternate_number, '_system');
          }
        }
      ]
    });
  });
});

app.onPageInit('login', function (page) {
  $$('body').on('click','#login-button', function(){
    var formData = app.formToJSON('#login-form');
    login(formData);
  });
});

app.onPageInit('register', function (page) {
  $$('body').on('click','#register-button', function(){
    var formData = app.formToJSON('#register-form');
    register(formData);
  });
});

$$('.menu').on('click', function () {
  showMenu();
});

function showMenu() {
  var buttons1 = [
    {
      text: html10n.get('contacts'),
      onClick: function () {
        $$('#map').css('z-index', 0);
        $$('#map-hide').hide();
        mainView.router.load({pageName: 'contacts'});
      }
    },
    {
      text: html10n.get('languages'),
      onClick: function () {
        app.modal({
          title: html10n.get('languages'),
          text: html10n.get('choose-language'),
          verticalButtons: true,
          buttons: [
            {
              text: 'English',
              onClick: function() {
                html10n.localize("en");
                localStorage.setItem('language', "en");
                initVoiceRecognition();
                saveLanguage();
              }
            },
            {
              text: 'Chinese, Simplified',
              onClick: function() {
                html10n.localize("zh-CN");
                localStorage.setItem('language', "zh-CN");
                initVoiceRecognition();
                saveLanguage();
              }
            },
            {
              text: 'Chinese, Traditional',
              onClick: function() {
                html10n.localize("zh-TW");
                localStorage.setItem('language', "zh-TW");
                initVoiceRecognition();
                saveLanguage();
              }
            },
            {
              text: 'Chinese, Traditional (Hong Kong)',
              onClick: function() {
                html10n.localize("zh-HK");
                localStorage.setItem('language', "zh-HK");
                initVoiceRecognition();
                saveLanguage();
              }
            },
          ]
        });
      }
    },
    {
      text: html10n.get('sign-out'),
      onClick: function () {
        app.confirm(html10n.get('are-you-sure'), html10n.get('sign-out'), function () {
          localStorage.clear();
          window.location.reload(true);
        });
      }
    }
  ];
  var buttons2 = [
    {
      text: html10n.get('cancel'),
      color: 'red'
    }
  ];
  var groups = [buttons1, buttons2];
  app.actions(groups);
}

function onDeviceReady() {
  if(localStorage.getItem('language') == null){
    html10n.localize(navigator.language);
    localStorage.setItem('language', navigator.language);
  }
  else {
    html10n.localize(localStorage.getItem('language'));
  }
  if(!localStorage.getItem("authenticated")) {
    mainView.router.load({pageName: 'login'});
  }
  else {
    mainView.router.load({pageName: 'index'});
    initMap();
    initGeolocation();
    initVoiceRecognition();
    sync();
  }
}

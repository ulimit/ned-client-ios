function bookmark(){
  var id = document.getElementById('route-id').getAttribute('route-id');
  app.prompt(html10n.get('name-this-journey'), html10n.get('save-journey'),
  function (value) {
    var xhttp = new XMLHttpRequest();
    var data =
    "owner="+ localStorage.getItem('id') +
    "&title="+ value +
    "&origin="+ localStorage.getItem("origin") +
    "&destination="+ localStorage.getItem("destination") +
    "&transitOptions="+ localStorage.getItem("transitOptions") +
    "&routeIndex="+ id;

    xhttp.onreadystatechange=function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
        app.hidePreloader();
        app.alert(html10n.get('bookmark-saved'), html10n.get('bookmark'));
        syncBookmarks();
      }
    };

    xhttp.open("POST", SERVER_URL +"/v1/api/bookmark/post", true);
    xhttp.setRequestHeader("Authorization", "Basic "+ btoa(localStorage.getItem("email") +":"+ localStorage.getItem("password")));
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(data);
    app.showPreloader(html10n.get('processing'));
  },
  function (value) {
    return;
  }
);
}

function loadBookmark(id){
  var bookmarks = JSON.parse(localStorage.getItem('bookmarks'));
  directionsDisplay.setMap(map);

  for (var i = 0; i < bookmarks.length; i++) {
    if (bookmarks[i].id == id) {
      var bookmarkIndex = i;

      directionsService.route({
        origin: {'placeId': bookmarks[i].origin},
        destination: {'placeId': bookmarks[i].destination},
        provideRouteAlternatives: true,
        travelMode: travel_mode,
        transitOptions: JSON.parse(bookmarks[i].transit_options)
      }, function(response, status, i) {
        if (status === google.maps.DirectionsStatus.OK) {
          document.getElementById('panel-left-content').innerHTML = '';
          directionsDisplay.setDirections(response);

          var route = response.routes;

          for(var i = 0; i < route.length; i++){
            localStorage.setItem("route"+ i, JSON.stringify(route[i]));
          }

          localStorage.setItem("routes", JSON.stringify(response.routes));

          if (isJourneyActive == true) {
            app.confirm(html10n.get('bookmark-journey-active'), 'NED',
              function () {
                mainView.router.load({pageName: 'steps'});
              },
              function () {
                return;
              }
            );
          }
          else {
            viewChosenRoute(JSON.parse(bookmarks[bookmarkIndex].route_index));
          }
        } else {
          window.alert('Directions request failed due to '+ status);
        }
      });
    }
  }
}

function deleteBookmark(id){
  app.modal({
    title: 'NED',
    text: html10n.get('bookmark-delete-confirm'),
    buttons: [
      {
        text: html10n.get('cancel'),
        onClick: function() {
          return;
        }
      },
      {
        text: html10n.get('ok'),
        onClick: function() {
          var xhttp = new XMLHttpRequest();

          xhttp.onreadystatechange=function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
              syncBookmarks();
            }
          };
          xhttp.open("POST", SERVER_URL +"/v1/api/bookmark/destroy/"+ id, true);
          xhttp.setRequestHeader("Authorization", "Basic "+ btoa(localStorage.getItem("email") +":"+ localStorage.getItem("password")));
          xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
          xhttp.send();
        }
      }
    ]
  });
}

function bookmarkToggleActions(id){
  var bookmark = document.getElementById(id);

  if (bookmark.classList.contains('swipeout-opened') === false) {
    app.swipeoutOpen('.bookmark-'+ id, 'right');
  }
  else {
    app.swipeoutClose('.bookmark-'+ id);
  }
}

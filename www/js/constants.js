var SERVER_URL = "http://ned-s3t-lamp-sc.cloudapp.net";

function getVehicleInfo(step, index){
  if (typeof step[index].transit !== "undefined") {
    var name;
    if (typeof step[index].transit.line.short_name !== "undefined") {
      name = step[index].transit.line.short_name;
    }
    else {
      name = step[index].transit.line.name;
    }

    var elements = {
      color: step[index].transit.line.color,
      text_color: step[index].transit.line.text_color,
      headsign: step[index].transit.headsign,
      name: step[index].transit.line.name,
      short_name: name,
      icon: step[index].transit.line.vehicle.icon,
      type: step[index].transit.line.vehicle.type
    };

    return JSON.parse(JSON.stringify(elements));
  }

  return JSON.parse(JSON.stringify({icon: "https://maps.gstatic.com/mapfiles/transit/iw2/6/walk.png"}));
}

function constructSteps(route, id, page){
  for (var i = 0; i < route.legs.length; i++) {
    var routeLeg = route.legs[i];
    var cards;

    for (var x = 0; x < routeLeg.steps.length; x++) {
      var routeLegSteps = routeLeg.steps;
      var routeLegStep = routeLeg.steps[x];
      var details = getVehicleInfo(routeLegSteps, x);

      if (x == 0) {
        var startAddress;

        if (routeLeg.start_address.indexOf(',') < 0) {
          startAddress = '<div class="item-title">'+ routeLeg.start_address +'</div>';
        }
        else {
          startAddress = '<div class="item-title">'+ routeLeg.start_address.substr(0, routeLeg.start_address.indexOf(',')) +'</div>';
        }

        cards =
        '<ul id="route-id" route-id="'+ id +'">'+
          '<li class="item-content">'+
            '<div class="item-inner">'+
              startAddress +
            '</div>'+
          '</li>';
      }

      if (typeof routeLegStep.steps == "undefined") {
        var shortName = '';
        var transit = '';
        var busStops = '';
        var refreshStop = '';

        if (typeof details.short_name !== "undefined"){
          var borderColor = details.color;
          if (borderColor == "#ffffff") {
            borderColor = "#777777";
          }
          shortName =
            '<span class="transit-line" style="color: '+ details.text_color +'; border-color: '+ borderColor +'; background-color: '+ details.color +';">'+
              details.short_name +
            '</span>'+
            '<span> - '+ details.headsign +'</span>';
        }

        //GET bus stops
        if (details.type == "BUS"){
          var xhttp = new XMLHttpRequest();
          var lat = routeLegStep.transit.departure_stop.location.lat;
          var lng = routeLegStep.transit.departure_stop.location.lng;
          var bus = routeLegStep.transit.line.short_name;
          var dep_name = routeLegStep.transit.departure_stop.name ;

          if(page == 'steps'){
            var arrTimeId = 'arr-time-'+ page +'-'+ x ;
            var wheelChairId = 'wheelchair-'+ page +'-'+ x ;
            refreshStop =
              '<span>'+ html10n.get('arrival-time') + routeLegStep.transit.departure_time.text +'</span>'+
              '<a href="javascript:getBusArrival(\''+ arrTimeId +'\',\''+ wheelChairId +'\','+ lat +','+ lng +','+ bus +',\''+ dep_name +'\');">'+
                '<i class="fa fa-fw fa-refresh"></i>'+
              '</a>'+
              '<div>'+
                '<span id="'+ arrTimeId +'" style="padding-left: 87px;">N/A</span>'+
                '<span id="'+ wheelChairId +'" style="color: #4cd964; display: none;"><i class="fa fa-fw fa-wheelchair"></i></span>'+
              '</div>';
          }

          var data = "?dep_lat="+ lat +
            "&dep_lng="+ lng +
            "&dep_name="+ dep_name +
            "&arr_lat="+ routeLegStep.transit.arrival_stop.location.lat +
            "&arr_lng="+ routeLegStep.transit.arrival_stop.location.lng +
            "&arr_name="+ routeLegStep.transit.arrival_stop.name +
            "&bus="+ bus;

          xhttp.onreadystatechange=function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
              var results = JSON.parse(xhttp.response);

              for (var y = 0; y < results.length; y++) {
                busStops += '<div class="item-subtitle">'+ results[y].bus_stop.description +'</div>';
              }
              transit = '<div class="item-stops" style="display:none;" id="transit-'+ page +'-'+ x +'">'+ busStops +'</div>';
            }
          };

          xhttp.open("GET", SERVER_URL +"/bus/inBetween"+ data, false);
          xhttp.setRequestHeader("Authorization", "Basic "+ btoa(localStorage.getItem("email") +":"+ localStorage.getItem("password")));
          xhttp.send();
        } else if (details.type == "SUBWAY" || details.type == "TRAM") {
          var xhttp = new XMLHttpRequest();
          var data = "?line="+ details.name +
            "&dep_station="+ routeLegStep.transit.departure_stop.name +
            "&arr_station="+ routeLegStep.transit.arrival_stop.name ;

          xhttp.onreadystatechange=function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
              var results = JSON.parse(xhttp.response);

              for (var y = 0; y < results.length; y++) {
                busStops += '<div class="item-subtitle">'+ results[y].name +'</div>';
              }
              transit = '<div class="item-stops" style="display:none;" id="transit-'+ page +'-'+ x +'">'+ busStops +'</div>';
            }
          };

          xhttp.open("GET", SERVER_URL +"/subway/inBetween"+ data, false);
          xhttp.setRequestHeader("Authorization", "Basic "+ btoa(localStorage.getItem("email") +":"+ localStorage.getItem("password")));
          xhttp.send();
        }

        cards +=
          '<li id="step-'+ x +'" class="item-content">'+
            '<div class="item-media" style="position: absolute; top: 24px;"><img class="view-route-img" src='+ details.icon +'></div>'+
            '<div class="item-inner" style="margin-left: 30px;">'+
              '<div class="item-title">'+ routeLegStep.transit.departure_stop.name +'</div>'+
              '<div class="item-subtitle">'+ shortName +'</div>'+
              '<div class="item-content item-content-inner">'+
                '<div class="item-inner" id="stop-details-'+ x +'">'+
                  '<div class="item-subtitle">'+
                    refreshStop +
                  '</div>'+
                  '<div class="item-text" >'+
                  '<a href="javascript:transitToggle(\''+ page +'-'+ x +'\')">'+
                    '<i id="arrow-down-'+ page + x +'" class="fa fa-fw fa-chevron-right"></i>'+
                    routeLegStep.transit.num_stops + html10n.get('stops') + routeLegStep.duration.text +')'+
                  '</a>'+
                  '</div>'+
                  transit +
                '</div>'+
              '</div>'+
              '<div class="item-title">'+ routeLegStep.transit.arrival_stop.name +'</div>'+
            '</div>'+
          '</li>';
      }
      else {
        for (var y = 0; y < routeLegStep.steps.length; y++) {
          var routeLegSubSteps = routeLegStep.steps;
          var routeLegSubStep = routeLegStep.steps[y];
          var instructions = '';

          if (routeLegSubSteps.length == 1) {
            instructions = '<div class="item-subtitle">'+ routeLegStep.instructions +'</div>';
          }
          else {
            if (typeof routeLegSubStep.instructions == "undefined") {
              instructions = '<div class="item-subtitle">Walk</div>';
            } else {
              instructions = '<div class="item-subtitle">'+ routeLegSubStep.instructions +'</div>';
            }
          }

          cards +=
            '<li id="step-'+ x +'-substep-'+ y +'" class="item-content">'+
              '<div class="item-media"><img class="view-route-img" src='+ details.icon +'></div>'+
              '<div class="item-inner">'+
                instructions +
                '<div class="item-text">'+ html10n.get('go-about') + routeLegSubStep.duration.text +', '+ routeLegSubStep.distance.text +'</div>'+
              '</div>'+
            '</li>';
        }
      }

      if (x == (routeLeg.steps.length - 1)) {
        var endAddress;

        if (routeLeg.end_address.indexOf(',') < 0) {
          endAddress = '<div class="item-title">'+ routeLeg.end_address +'</div>';
        }
        else {
          endAddress = '<div class="item-title">'+ routeLeg.end_address.substr(0, routeLeg.end_address.indexOf(',')) +'</div>';
        }

        cards +=
          '<li class="item-content">'+
            '<div class="item-inner">'+
              endAddress +
            '</div>'+
          '</li>'+
        '</ul>';
      }

      $$('#'+ page +'-content').html(cards)
      app.hideIndicator();
      mainView.router.load({pageName: page});
    }
  }
}

function getBusArrival(arrTimeId, wheelChairId, lat, lng,bus,stop_name){
  var busxhttp = new XMLHttpRequest();
  var busdata = "?bus="+ bus +
    "&lat="+ lat +
    "&lng="+ lng +
    "&stop_name="+ stop_name;

  //Bus stop arrival
  busxhttp.onreadystatechange=function() {
    if (busxhttp.readyState == 4 && busxhttp.status == 200) {
      var busresults = JSON.parse(busxhttp.response);
      console.log(busresults);
      if (busresults.NextBus.Feature == "WAB") {
        $$('#'+ wheelChairId).css('display', 'inline');
      } else {
        $$('#'+ wheelChairId).hide();
      }
      $$('#'+ arrTimeId).html(moment(busresults.NextBus.EstimatedArrival).format("h:mm a"));
    }
  };

  busxhttp.open("GET", SERVER_URL +"/bus/arrival"+ busdata, true);
  busxhttp.setRequestHeader("Authorization", "Basic "+ btoa(localStorage.getItem("email") +":"+ localStorage.getItem("password")));
  busxhttp.send();
}

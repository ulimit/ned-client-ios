function initGeolocation(){
  app.showIndicator();

  backgroundGeolocation.configure(function(){}, function(){}, {
    desiredAccuracy: 10,
    stationaryRadius: 5,
    distanceFilter: 5,
  });

  backgroundGeolocation.start();
  navigator.geolocation.getCurrentPosition(initSuccess, initError, {timeout: 30000, maximumAge: 0, enableHighAccuracy: true});

  function initSuccess(position) {
    var location = position.coords;

    origin = new google.maps.LatLng(location.latitude, location.longitude);

    currentLng = location.latitude;
    currentLat = location.longitude;

    map.setCenter(new google.maps.LatLng(location.latitude, location.longitude));
    setCurrentLocationMarker(location);
    setLocationPolyline(location);

    origin_id = null;

    app.hideIndicator();
    backgroundGeolocation.finish();
    backgroundGeolocation.stop();
  }

  function initError(error) {
    app.hideIndicator();
    backgroundGeolocation.finish();
    backgroundGeolocation.stop();

    app.modal({
      title: html10n.get('error-code', { code: error.code}),
      text: error.message,
      buttons: [
        {
          text: html10n.get('retry'),
          onClick: function() {
            initGeolocation();
          }
        }
      ]
    });
  }
}

function initWatchGeolocation(id){
  backgroundGeolocation.configure(initWatchGeolocationcallbackFn, initWatchGeolocationfailureFn, {
    desiredAccuracy: 10,
    stationaryRadius: 5,
    distanceFilter: 5,
    debug: false, // <-- enable this hear sounds for background-geolocation life-cycle.
    saveBatteryOnBackground: false, // <-- iOS: If enabled it will automatically switch to less accurate significant changes and region monitory when in background.
    activityType: 'OtherNavigation', // iOS: Apple docs (CLActivityType) for more information.
    locationProvider: backgroundGeolocation.provider.ANDROID_ACTIVITY_PROVIDER,
    interval: 15000, // <-- Android: Rate in milliseconds at which your app prefers to receive location updates.
    fastestInterval: 5000, // <-- Android: Fastest rate in milliseconds at which your app can handle location updates.
  });

  backgroundGeolocation.start();

  function initWatchGeolocationcallbackFn(location) {
    origin = new google.maps.LatLng(location.latitude, location.longitude);

    currentLng = location.latitude;
    currentLat = location.longitude;

    map.setCenter(new google.maps.LatLng(location.latitude, location.longitude));
    setCurrentLocationMarker(location);
    setLocationPolyline(location);
    navigate(id);

    backgroundGeolocation.finish();
  }

  function initWatchGeolocationfailureFn(error) {
    console.log('Watch geolocation failed. Continuing watch. error: '+ error);
  }
}

function initGeolocationThenGo(){
  app.showIndicator();

  navigator.geolocation.getCurrentPosition(initSuccess, initError, {timeout: 30000, maximumAge: 0, enableHighAccuracy: true});

  function initSuccess(position) {
    var location = position.coords;

    origin = new google.maps.LatLng(location.latitude, location.longitude);

    currentLng = location.latitude;
    currentLat = location.longitude;

    map.setCenter(new google.maps.LatLng(location.latitude, location.longitude));
    setCurrentLocationMarker(location);

    go();
    app.hideIndicator();
  }

  function initError(error) {
    app.hideIndicator();

    app.modal({
      title: html10n.get('error-code', { code: error.code}),
      text: error.message,
      buttons: [
        {
          text: html10n.get('retry'),
          onClick: function() {
            initGeolocationThenGo();
          }
        }
      ]
    });
  }
}

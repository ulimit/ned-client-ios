var markers = [];
var infowindow = new google.maps.InfoWindow();

function nearby(data) {
  var service = new google.maps.places.PlacesService(map);
  deleteMarkers();
  document.getElementById('nearby-list').firstElementChild.innerHTML = '';

  for (var i = 0; i < data.type.length; i++) {
    var request = {
      location: origin,
      radius: '1000',
      //rankBy: google.maps.places.RankBy.DISTANCE,
      type: data.type[i]
    };
    service.nearbySearch(request, callback);
  }

  if (data.type.length == 0) {
    return;
  }

  app.showIndicator();
  mainView.router.load({pageName: 'nearby-results'});
}

function nearbyEvent(){
  var xhttp = new XMLHttpRequest();
  var data = {long: currentLng, lat: currentLat};

  xhttp.onreadystatechange=function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      displayNearbyEvent(xhttp.response);
    }
  };
  xhttp.open("GET", SERVER_URL +"/v1/api/events/list?lat="+ currentLat +"&long="+ currentLng , true);
  xhttp.setRequestHeader("Authorization", "Basic "+ btoa(localStorage.getItem("email") +":"+ localStorage.getItem("password")));
  xhttp.send();

  function displayNearbyEvent(result) {
    var result = JSON.parse(result),
        content = result.events,
        html = '',
        i = 0;

    html += '<div class="list-block">';
    html += '<ul style="background-color: rgba(255,255,255,0.5)" id="nearby-events-list">';

    for(i = 0; i < result.events.length; i++){

      html += '<li>';
        html += '<div class="card demo-card-header-pic">';
          html += '<div style="background-color: black; height: 100px" valign="bottom" class="card-header color-white no-border">'+ content[i].name.text +'</div>';
          html += '<div class="card-content">';
            html += '<div class="card-content-inner">';

              html += '<p class="color-gray">From '+ moment(content[i].start.utc).format("MMMM D YYYY, h:mm a") +'</p>';
              html += '<p style="margin-top: -16px" class="color-gray">To '+ moment(content[i].end.utc, 'YYYY-MM-DD HH:mm Z').format("MMMM D YYYY, h:mm a") +'</p>';

              html += '<p style="max-height:98px;overflow:hidden;text-overflow:ellipsis" id="desc_'+ i +'">'+ content[i].description.text +'</p>';
            html += '</div>';
          html += '</div>';
          html += '<div class="card-footer">';
            html += '<a href="javascript: window.open(\''+ content[i].url +'\', \'_system\');" class="link">View Link</a>';
            html += '<a id="readToggle_'+ i +'" href="javascript:readMore('+ i +')" class="link">Read More</a>';
          html += '</div>';
        html += '</div>';
      html += '</li>';
    }

    html += '</ul>';
    html += '</div>';

    if(result.events.length < 1){
      html = '<div class="card"><div class="card-content"><div class="card-content-inner" data-l10n-id="no-events">'+ html10n.get('no-events') +'</div></div></div>';
    }

    document.getElementById('nearby-event').innerHTML = html;

    app.hideIndicator();
  }
}

function readMore(i){
  $$("#desc_"+ i).css('max-height','none');
  $$("#readToggle_"+ i).attr('href','javascript: readLess('+ i +')');
  $$("#readToggle_"+ i).text("Read Less");
}

function readLess(i){
  $$("#desc_"+ i).css('max-height','98px');
  $$("#readToggle_"+ i).attr('href','javascript: readMore('+ i +')');
  $$("#readToggle_"+ i).text("Read More");
}

function callback(results, status) {
  if (status == google.maps.places.PlacesServiceStatus.OK) {

    for (var i = 0; i < results.length; i++) {
      createMarker(results[i]);
      document.getElementById('nearby-list').firstElementChild.innerHTML +=
        '<li>'+ 
          '<a href="javascript: speak(\''+ results[i].name +' '+ results[i].vicinity +'\'); getNearbyRoute(\''+ results[i].place_id +'\' , \''+ results[i].name +'\');" class="item-content">'+ 
            '<div class="item-inner">'+ 
              '<div class="item-title-row">'+ 
                '<div class="item-title">'+ results[i].name +'</div>'+ 
              '</div>'+ 
              '<div class="item-text">'+ results[i].vicinity +'</div>'+ 
            '</div>'+ 
          '</a>'+ 
        '</li>';

      if (i == (results.length - 1)) {
        app.hideIndicator();
      }
    }
  }
  else {
    app.hideIndicator();
    app.closeModal();
    app.alert(html10n.get('no-results'), 'NED');
  }
}

function createMarker(place) {
  var marker = new google.maps.Marker({
    map: map,
    position: place.geometry.location
  });

  markers.push(marker);

  if (place.geometry.viewport) {
    map.fitBounds(place.geometry.viewport);
  } else {
    map.setCenter(origin);
    map.setZoom(14);
  }
  google.maps.event.addListener(marker, 'click', function() {
    infowindow.setContent(place.name);
    infowindow.open(map, this);
  });
}

function deleteMarkers() {
  clearMarkers();
  markers = [];
}

function clearMarkers() {
  setMapOnAll(null);
}

function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

function getNearbyRoute(placeId, placeName){
  app.modal({
    title: 'NED',
    text: html10n.get('get-nearby-route-confirm'),
    buttons: [
      {
        text: html10n.get('cancel'),
        onClick: function() {
          return;
        }
      },
      {
        text: html10n.get('ok'),
        onClick: function() {
          document.getElementById('go-origin').value = '';
          document.getElementById('go-destination').value = placeName;
          document.getElementById('route-list').innerHTML = '';
          origin_id = null;
          destination_id = placeId;
          initGeolocationThenGo();
          mainView.router.load({pageName: 'go'});
        }
      }
    ]
  });
}

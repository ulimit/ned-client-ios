function uploadJourney(id){
  var routes = JSON.parse(localStorage.getItem('routes'));

  var watchRoute = routes[id];
  var startAddress, endAddress, uploadJourneyIntervalId;

  for (var i = 0; i < watchRoute.legs.length; i++) {
    var watchRouteLeg = watchRoute.legs[i];

    if (watchRouteLeg.start_address.indexOf(',') < 0) {
      startAddress = watchRouteLeg.start_address;
    }
    else {
      startAddress = watchRouteLeg.start_address.substr(0, watchRouteLeg.start_address.indexOf(','));
    }

    if (watchRouteLeg.end_address.indexOf(',') < 0) {
      endAddress = watchRouteLeg.end_address;
    }
    else {
      endAddress = watchRouteLeg.end_address.substr(0, watchRouteLeg.end_address.indexOf(','));
    }
  }

  var xhttp = new XMLHttpRequest();
  var data = "owner="+ localStorage.getItem('id') +"&journeyId="+ journeyId +"&start="+ startAddress +"&end="+ endAddress;

  xhttp.onreadystatechange=function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      clearInterval(uploadJourneyIntervalId);
    }
  };

  uploadJourneyIntervalId = setInterval(
    function(){
      xhttp.open("POST", SERVER_URL +"/v1/api/journey/post", true);
      xhttp.setRequestHeader("Authorization", "Basic "+ btoa(localStorage.getItem("email") +":"+ localStorage.getItem("password")));
      xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      xhttp.send(data);
    },
    5000
  );
}

function uploadLogs(){
  var journey = JSON.parse(localStorage.getItem('journey'));
  if(journey === null) return;

  var xhttp = new XMLHttpRequest();
  var data = "owner="+ localStorage.getItem('id') +"&journey="+ localStorage.getItem('journey');

  xhttp.onreadystatechange=function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      localStorage.removeItem('journey');
    }
  };
  xhttp.open("POST", SERVER_URL +"/v1/api/log/post", true);
  xhttp.setRequestHeader("Authorization", "Basic "+ btoa(localStorage.getItem("email") +":"+ localStorage.getItem("password")));
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(data);
}

function loadJourney(id){
  app.showIndicator();
  var xhttp = new XMLHttpRequest();
  var loadJourneyIntervalId;

  xhttp.onreadystatechange=function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      localStorage.setItem('logs', xhttp.response);
      clearInterval(loadJourneyIntervalId);
      displayJourney();
    }
  };

  loadJourneyIntervalId = setInterval(
    function(){
      xhttp.open("GET", SERVER_URL +"/v1/api/log/list?owner="+ localStorage.getItem('id') +"&journeyId="+ id, true);
      xhttp.setRequestHeader("Authorization", "Basic "+ btoa(localStorage.getItem("email") +":"+ localStorage.getItem("password")));
      xhttp.send();
    },
    5000
  );

  function displayJourney(){
    var logs = JSON.parse(localStorage.getItem('logs'));
    var journey = [];

    if (!polyline) {
      polyline = new google.maps.Polyline({
        zIndex: 1,
        map: map,
        geodesic: true,
        strokeColor: '#2677FF',
        strokeOpacity: 0.7,
        strokeWeight: 5
      });
    }
    else {
      polyline.setMap(null);
    }

    if (!startMarker && !endMarker) {
      startMarker = new google.maps.Marker({
        map:map
      });

      endMarker =  new google.maps.Marker({
        map:map
      });
    }
    else {
      startMarker.setMap(null);
      endMarker.setMap(null);
    }

    for (var i = 0; i < logs.length; i++) {
      var journeyArray = JSON.parse(logs[i].journey);

      for (var x = 0; x < journeyArray.length; x++) {
        journey.push(new google.maps.LatLng(journeyArray[x].lat, journeyArray[x].lng));
      }
    }

    polyline.setMap(map);
    startMarker.setMap(map);
    endMarker.setMap(map);

    polyline.setPath(journey);
    startMarker.setPosition(polyline.getPath().getAt(0));
    endMarker.setPosition(polyline.getPath().getAt(polyline.getPath().getLength()-1));

    map.setCenter(journey[0]);
    showMap();
    app.hideIndicator();
  }
}

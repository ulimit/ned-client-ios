document.addEventListener('deviceready', onDeviceReady, false);

function initVoiceRecognition() {
  console.log(localStorage.getItem('language'));
  ApiAIPlugin.init(
    {
      clientAccessToken: "REPLACE_ME_WITH_YOUR_API_KEY", // insert your client access key here
      lang: localStorage.getItem('language') // set lang tag from list of supported languages
    },
    function(result) { console.log('Api.ai init success') },
    function(error) { console.log('Api.ai init error: '+ error) }
  );
}

function sendVoice() {
  try {
    ApiAIPlugin.setListeningStartCallback(function () {
      console.log('start-listening');
      app.modal({
        title: html10n.get('speak-now'),
        text: '<i class="fa fa-microphone" style="font-size: 56px;"></i>',
        buttons: [
          {
            text: html10n.get('cancel'),
            bold: true,
            onClick: function() {
              ApiAIPlugin.cancelAllRequests();
              return;
            }
          },
          {
            text: html10n.get('ok'),
            bold: true,
            onClick: function() {
              ApiAIPlugin.stopListening();
              return;
            }
          },
        ]
      });
    });

    ApiAIPlugin.setListeningFinishCallback(function () {
      console.log('stop-listening');
      app.closeModal();
      app.showPreloader(html10n.get('processing'));
    });

    ApiAIPlugin.requestVoice(
      {}, // empty for simple requests, some optional parameters can be here
      function (response) {
        app.hidePreloader();
        voiceCommands(response.result.resolvedQuery);
      },
      function (error) {
        app.hidePreloader();
        app.modal({
          title: 'NED',
          text: error,
          buttons: [
            {
              text: html10n.get('ok'),
              bold: true,
              onClick: function() {
                return;
              }
            }
          ]
        });
      }
    );
  } catch (e) {
    app.modal({
      title: 'NED',
      text: e,
      buttons: [
        {
          text: html10n.get('ok'),
          bold: true,
          onClick: function() {
            return;
          }
        }
      ]
    });
  }
}

function speak(message){
  TTSNotification.speak({title: 'NED', icon: 'icon', message: message, language: localStorage.getItem('language')}, function () {
    console.log('Speak success');
  }, function (reason) {
    console.log('Speak failed. error: '+ reason);
  });
}

//Voice commands
function voiceCommands(commands){
  var firstWord = commands.toLowerCase();
  console.log(firstWord);

//Voice commands triggers
  switch (firstWord) {
    //You can add chinese command triggers for each case ex.:
    // case "ni hao ma":
    //   console.log('Hello');
    // break;
    case "home":
      mainView.router.load({pageName: 'index'});
    break;
    case "menu":
      showMenu();
    break;
    case "back":
      mainView.router.back({pageName: 'index', force: true});
    break;
    case "direction":
    case "directions":
      goJourneyCheck();
    break;
    case "nearby":
    case "near by":
      mainView.router.load({pageName: 'nearby'});
    break;
    case "call":
      callContacts();
    break;
    case "history":
    case "history of journey":
      mainView.router.load({pageName: 'history'});
    break;
    case "book mark":
    case "book mark list":
    case "bookmark":
    case "bookmark list":
      mainView.router.load({pageName: 'bookmarks'});
    break;
    case "map":
    case "show map":
    case "hide map":
      showMap();
    break;
    case "go":
      initGeolocationThenGo();
    break;
    case "book my journey":
    case "book mark journey":
    case "bookmark journey":
    case "bookmark the journey":
      bookmark();
    break;
    case "start journey":
      startJourney();
    break;
    case "end journey":
      endJourney();
    break;
    case "nearby location":
    case "nearby locations":
      $$('#nearby-button').click();
    break;
    case "nearby event":
    case "nearby events":
      $$('#nearby-eve').click();
    break;
  }
}

function listen(){
  sendVoice();
}

function goListen(id){
  try {
    ApiAIPlugin.requestVoice(
      {}, // empty for simple requests, some optional parameters can be here
      function (response) {
        app.hidePreloader();
        document.getElementById(id).value = response.result.resolvedQuery;
        document.getElementById(id).focus();
      },
      function (error) {
        app.hidePreloader();
        app.modal({
          title: 'NED',
          text: error,
          buttons: [
            {
              text: html10n.get('ok'),
              bold: true,
              onClick: function() {
                return;
              }
            }
          ]
        });
      }
    );
  } catch (e) {
    app.modal({
      title: 'NED',
      text: e,
      buttons: [
        {
          text: html10n.get('ok'),
          bold: true,
          onClick: function() {
            return;
          }
        }
      ]
    });
  }
}

function sendSMS(sender, number, message){
  var request = {'location': origin};

  geocoder.geocode(request, function(results) {
    if (results) {
      var data =
        "appKey=S32016SMS02INNO26"+ 
        "&toMobile="+ number +
        "&recipientName="+ sender +
        "&senderID=NeD"+ 
        "&smsText="+ message + localStorage.getItem('name') + html10n.get('is-at') + results[0].formatted_address +
        "&taskName=alert";
    }

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange=function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
        var response = JSON.parse(JSON.stringify(xhttp.response));
      }
    };

    xhttp.open("POST", SERVER_URL +"/v1/api/sendsms", true);
    xhttp.setRequestHeader("Authorization", "Basic "+ btoa(localStorage.getItem("email") +":"+ localStorage.getItem("password")));
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(data);
  });
}

function editContact(type,contact_person, contact_number){
  //alert("call edit contact");
  document.getElementById('contact_type').value=type;
  document.getElementById('contact_name').value=contact_person;
  document.getElementById('contact_number').value=contact_number;
  mainView.router.load({pageName: 'contacts-edit'});
}

function saveContact(data){
  var xhttp = new XMLHttpRequest();
  var value = "type="+ data.contact_type + 
              "&contact_number="+ data.contact_number + 
              "&contact_name="+ data.contact_name + 
              "&owner="+ localStorage.getItem('id');

  xhttp.onreadystatechange=function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var contacts = JSON.parse(localStorage.getItem('contacts'));
      if(data.contact_type == "main"){
        contacts.contact = data.contact_name;
        contacts.contact_number = data.contact_number;  
      }else{
        contacts.alternate = data.contact_name;
        contacts.alternate_number = data.contact_number; 
      }
      localStorage.setItem('contacts', JSON.stringify(contacts));
      document.getElementById('main-contact').innerText = contacts.contact;
      document.getElementById('main-contact-number').innerText = contacts.contact_number;
      document.getElementById('alternate-contact').innerText = contacts.alternate;
      document.getElementById('alternate-contact-number').innerText = contacts.alternate_number;
      app.hidePreloader();
      mainView.router.back();//load({pageName: 'contacts'});
    }
  };
  app.showPreloader('Saving contact .....');
  if(data.contact_number && data.contact_name !== "") {
    xhttp.open("POST", SERVER_URL +"/v1/api/contact", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.setRequestHeader("Authorization", "Basic "+ btoa(localStorage.getItem("email") +":"+ localStorage.getItem("password")));
    xhttp.send(value);
  }
  else {
    //app.hidePreloader();
    app.alert(html10n.get('alert-contacct'), html10n.get('contact-failed'));
  }
}

function callContacts(){
  var contacts = JSON.parse(localStorage.getItem('contacts'));

  sendSMS(
    localStorage.getItem('name'),
    contacts.contact_number,
    html10n.get('call-sms-msg', { contact: contacts.contact, name: localStorage.getItem('name')}));

  cordova.InAppBrowser.open('tel:'+ contacts.contact_number, '_system');

  mainView.router.load({pageName: 'contacts'});
}

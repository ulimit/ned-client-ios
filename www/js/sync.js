function sync() {
  setInterval(
    function(){
      syncContacts();
    },
    600000
  );
  syncLanguage();
  syncContacts();
  syncBookmarks();
  syncHistory();
}

function syncLanguage() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange=function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var res = JSON.parse(xhttp.response)
      if (res.lang !== "") {
        localStorage.setItem('language', res.lang);
      }
    }
  };
  xhttp.open("GET", SERVER_URL +"/v1/api/contacts?owner="+ localStorage.getItem('id'), true);
  xhttp.setRequestHeader("Authorization", "Basic "+ btoa(localStorage.getItem("email") +":"+ localStorage.getItem("password")));
  xhttp.send();
}

function saveLanguage() {
  var xhttp = new XMLHttpRequest();
  var data =
  "owner="+ localStorage.getItem('id') +
  "&lang="+ localStorage.getItem('language');

  xhttp.onreadystatechange=function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var res = JSON.parse(xhttp.response);
    }
  };
  xhttp.open("POST", SERVER_URL +"/v1/api/setting/lang", true);
  xhttp.setRequestHeader("Authorization", "Basic "+ btoa(localStorage.getItem("email") +":"+ localStorage.getItem("password")));
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(data);
}

function syncContacts() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange=function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      localStorage.setItem('contacts', xhttp.response);
      displayContacts();
    }
  };
  xhttp.open("GET", SERVER_URL +"/v1/api/contacts?owner="+ localStorage.getItem('id'), true);
  xhttp.setRequestHeader("Authorization", "Basic "+ btoa(localStorage.getItem("email") +":"+ localStorage.getItem("password")));
  xhttp.send();

  function displayContacts(){
    var contacts = JSON.parse(localStorage.getItem('contacts'));
    $$('#main-contact').text(contacts.contact);
    $$('#main-contact-number').text(contacts.contact_number);
    $$('#alternate-contact').text(contacts.alternate);
    $$('#alternate-contact-number').text(contacts.alternate_number);
  }
}

function syncBookmarks(){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange=function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      localStorage.setItem('bookmarks', xhttp.response);
      displayBookmarks();
    }
  };
  xhttp.open("GET", SERVER_URL +"/v1/api/bookmark/list?owner="+ localStorage.getItem('id'), true);
  xhttp.setRequestHeader("Authorization", "Basic "+ btoa(localStorage.getItem("email") +":"+ localStorage.getItem("password")));
  xhttp.send();

  function displayBookmarks(){
    var bookmarks = JSON.parse(localStorage.getItem('bookmarks'));
    document.getElementById('bookmarks-list').innerHTML = '';

    for (var i = 0; i < bookmarks.length; i++) {
      document.getElementById('bookmarks-list').innerHTML +=
        '<li id="'+ bookmarks[i].id +'" class="swipeout bookmark-'+ bookmarks[i].id +'">'+
          '<div class="swipeout-content">'+
            '<a href="javascript: bookmarkToggleActions('+ bookmarks[i].id +');" class="item-content item-link">'+
              '<div class="item-inner">'+
                '<div class="item-title-row">'+
                  '<div class="item-title">'+ bookmarks[i].title +'</div>'+
                '</div>'+
                '<div class="item-subtitle">'+ bookmarks[i].formatted_origin +'</div>'+
                '<div class="item-text">'+ bookmarks[i].formatted_destination +'</div>'+
              '</div>'+
            '</a>'+
          '</div>'+
          '<div class="swipeout-actions-right">'+
            '<a href="#" onclick="deleteBookmark('+ bookmarks[i].id +');" class="bg-red" data-l10n-id="delete">'+ html10n.get('delete') +'</a>'+
            '<a href="#" onclick="loadBookmark('+ bookmarks[i].id +');" class="bg-green swipeout-overswipe" data-l10n-id="load">'+ html10n.get('load') +'</a>'+
          '</div>'+
        '</li>';
    }
  }
}

function syncHistory(){
  var xhttp = new XMLHttpRequest();

  xhttp.onreadystatechange=function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      localStorage.setItem('journeys', xhttp.response);
      displayHistory();
    }
  };
  xhttp.open("GET", SERVER_URL +"/v1/api/journey/list?owner="+ localStorage.getItem('id'), true);
  xhttp.setRequestHeader("Authorization", "Basic "+ btoa(localStorage.getItem("email") +":"+ localStorage.getItem("password")));
  xhttp.send();

  function displayHistory(){
    var journeys = JSON.parse(localStorage.getItem('journeys'));
    document.getElementById('history-list').innerHTML = '';

    for (var i = 0; i < journeys.length; i++) {
      var date = new Date(Number(journeys[i].journeyId));
      document.getElementById('history-list').innerHTML +=
        '<li>'+
          '<a href="javascript: loadJourney('+ journeys[i].journeyId +');" class="item-content item-link">'+
            '<div class="item-inner">'+
              '<div class="item-title-row">'+
                '<div class="item-title">'+ moment(date).format("MMMM D YYYY, h:mm a") +'</div>'+
              '</div>'+
              '<div class="item-subtitle">'+ journeys[i].start +'</div>'+
              '<div class="item-text">'+ journeys[i].end +'</div>'+
            '</div>'+
          '</a>'+
        '</li>';
    }
  }
}

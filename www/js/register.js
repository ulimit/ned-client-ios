function register(data){
  var xhttp = new XMLHttpRequest();
  var value = "email="+ data.email +"&password="+ data.password +"&name="+ data.name;
  xhttp.onreadystatechange=function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      app.hidePreloader();
      mainView.router.load({pageName: 'login'});
    }
  };
  app.showPreloader('Validating');
  if(data.password !== data.confirm) {
    app.hidePreloader();
    app.alert(html10n.get('password-mismatch'), html10n.get('registration-failed'));
  }
  else if(data.name && data.email && data.password && data.confirm !== "") {
    xhttp.open("POST", SERVER_URL +"/v1/api/register", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(value);
  }
  else {
    app.hidePreloader();
    app.alert(html10n.get('alert-register'), html10n.get('registration-failed'));
  }
}

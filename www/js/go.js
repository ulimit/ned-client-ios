var journeyId = null;
var isJourneyActive = false;
var origin_id, destination_id, routing_preference  = null;
var travel_mode = google.maps.TravelMode.TRANSIT;
var directionsService = new google.maps.DirectionsService;
var directionsDisplay = new google.maps.DirectionsRenderer;

function initGo() {
  directionsDisplay.setMap(map);
  directionsDisplay.setPanel(document.getElementById('panel-left-content'));

  var go_origin = document.getElementById('go-origin');
  var go_destination = document.getElementById('go-destination');

  var options = {
    componentRestrictions: {country: "SG"}
  };

  var origin_autocomplete = new google.maps.places.Autocomplete(go_origin, options);
  origin_autocomplete.bindTo('bounds', map);
  var destination_autocomplete = new google.maps.places.Autocomplete(go_destination, options);
  destination_autocomplete.bindTo('bounds', map);

  function expandViewportToFitPlace(map, place) {
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);
    }
  }

  origin_autocomplete.addListener('place_changed', function() {
    var place = origin_autocomplete.getPlace();

    if (!place.geometry) {
      window.alert("Autocomplete's returned place contains no geometry");
      return;
    }
    expandViewportToFitPlace(map, place);

    origin_id = place.place_id;
  });

  destination_autocomplete.addListener('place_changed', function() {
    var place = destination_autocomplete.getPlace();

    if (!place.geometry) {
      window.alert("Autocomplete's returned place contains no geometry");
      return;
    }
    expandViewportToFitPlace(map, place);

    destination_id = place.place_id;
  });
}

function clearInput(id){
  document.getElementById(id).value = '';
  document.getElementById(id).focus();
  document.getElementById('route-list').innerHTML = '';

  switch (id) {
    case 'go-origin':
      origin_id = null;
      break;
    case 'go-destination':
      destination_id = null;
      break;
    default:
      return;
  }
}

function go() {
  if (!destination_id) {
    return;
  }

  directionsService.route({
    origin: (!origin_id ? origin: {'placeId': origin_id}),
    destination: {'placeId': destination_id},
    provideRouteAlternatives: true,
    travelMode: travel_mode
  }, function(response, status) {
    if (status === google.maps.DirectionsStatus.OK) {
      document.getElementById('panel-left-content').innerHTML = '';
      directionsDisplay.setDirections(response);

      var route = response.routes;
      var recCards =
        '<ul>'+
          '<li class="item-content" style="background-color: #EEE;">'+
            '<div class="item-inner">'+
              '<div class="item-title" data-l10n-id="recommended-route">'+ html10n.get('recommended-route') +'</div>'+
            '</div>'+
          '</li>'+
        '</ul>';
      var otherCards =
        '<ul>'+
          '<li class="item-content" style="background-color: #EEE;">'+
            '<div class="item-inner">'+
              '<div class="item-title" data-l10n-id="other-options">'+ html10n.get('other-options') +'</div>'+
            '</div>'+
          '</li>'+
        '</ul>'+
        '<ul>';

      for(var i = 0; i < route.length; i++){
        for (var x = 0; x < route[i].legs.length; x++) {
          var leg = route[i].legs[x];
          var media = '';
          var title = '';

          for(var a = 0; a < leg.steps.length; a++){
            var shortName = '';
            var nextIcon = '';
            var steps = leg.steps;
            var details = getVehicleInfo(steps, a);

            if (a < (steps.length - 1)) {
              nextIcon = '<img class="route-img" src="./img/arrow-right.png">';
            }

            if (typeof details.short_name !== "undefined"){
              shortName =
                '<span class="transit-line" style="color: '+ details.text_color +'; border-color: '+ details.color +'; background-color: '+ details.color +';">'+
                  details.short_name +
                '</span>';
            }

            media +=
              '<div class="item-media"><img class="route-img" src='+ details.icon +'>'+
                shortName +
                nextIcon +
              '</div>';
          }

          if (i == 0) {
            recCards +=
              '<ul>'+
                '<li>'+
                  '<a href="javascript: viewChosenRoute('+ i +');" class="item-content">'+
                    '<div class="item-inner">'+
                      '<div class="item-title-row">'+
                        '<div class="item-inner-media">'+ media +'</div>'+
                        '<div class="item-after">'+ leg.duration.text +'</div>'+
                      '</div>'+
                    '</div>'+
                  '</a>'+
                '</li>'+
              '</ul>';
          } else {
            otherCards +=
              '<li>'+
                '<a href="javascript: viewChosenRoute('+ i +');" class="item-content">'+
                  '<div class="item-inner">'+
                    '<div class="item-title-row">'+
                      '<div class="item-inner-media">'+ media +'</div>'+
                      '<div class="item-after">'+ leg.duration.text +'</div>'+
                    '</div>'+
                  '</div>'+
                '</a>'+
              '</li>';
          }
        }
      }

      otherCards += '</ul>';
      var cards = recCards + otherCards;

      document.getElementById('route-list').innerHTML = cards;

      localStorage.setItem("routes", JSON.stringify(response.routes));
      localStorage.setItem("origin", JSON.stringify(!origin_id ? origin: {'placeId': origin_id}));
      localStorage.setItem("destination", JSON.stringify({'placeId': destination_id}));

    } else {
      window.alert('Directions request failed due to '+ status);
    }
  });
}

function viewChosenRoute(id){
  app.showIndicator();
  setTimeout(
    function(){
      var routes = JSON.parse(localStorage.getItem('routes'));
      constructSteps(routes[id], id, 'route');
      getAllElementsWithAttribute(id);
    },
    500
  );
}

function transitToggle(id){
  if ($$('#transit-'+ id).css('display') == 'none') {
    $$('#transit-'+ id).show();
    $$('#arrow-down-'+ id).removeClass('fa-chevron-right');
    $$('#arrow-down-'+ id).addClass('fa-chevron-down');
  } else {
    $$('#transit-'+ id).hide();
    $$('#arrow-down-'+ id).removeClass('fa-chevron-down');
    $$('#arrow-down-'+ id).addClass('fa-chevron-right');
  }
}

function goJourneyCheck() {
  if (isJourneyActive == true) {
    mainView.router.load({pageName: 'steps'});
  }
  else {
    mainView.router.load({pageName: 'go'});
  }
}

function getAllElementsWithAttribute(index){
  setTimeout(
    function(){
      var elements = document.getElementsByTagName('li');

      var foundelements = [];

      for (var i = 0; i < elements.length; i++) {

        if (elements[i].attributes.length > 0) {
          for (var x = 0; x < elements[i].attributes.length; x++) {

            if (elements[i].attributes[x].name === 'data-route-index') {
              foundelements.push(elements[i]);
            }
          }
        }
      }

      if (typeof foundelements[index] == "undefined") {
        app.hideIndicator();
      }
      else {
        foundelements[index].click(app.hideIndicator());
      }
    },
    1000
  );
}

function startNavigateInit(id){
  app.showIndicator();
  setTimeout(
    function(){
      var routes = JSON.parse(localStorage.getItem('routes'));
      constructSteps(routes[id], id, 'steps');
      initWatchGeolocation(id);
    },
    500
  );
}

function startJourney(){
  var id = document.getElementById('route-id').getAttribute('route-id');
  app.modal({
    title: 'NED',
    text: html10n.get('start-journey-confirm'),
    buttons: [
      {
        text: html10n.get('cancel'),
        onClick: function() {
          return;
        }
      },
      {
        text: html10n.get('ok'),
        onClick: function() {
          uploadLogs();
          journeyId = Date.now();
          journey(id);
          isJourneyActive = true;
          uploadJourney(id);
          startNavigateInit(id);
        }
      }
    ]
  });
}

function endJourney(){
  app.modal({
    title: 'NED',
    text: html10n.get('start-journey-confirm'),
    buttons: [
      {
        text: html10n.get('cancel'),
        onClick: function() {
          return;
        }
      },
      {
        text: html10n.get('ok'),
        onClick: function() {
          backgroundGeolocation.stop();
          syncHistory();
          isJourneyActive = false;
          clearInterval(geofenceIntervalId);
          document.getElementById('route-list').innerHTML = '';
          mainView.router.load({pageName: 'go'});
          journeyId = null;
          uploadLogs();
          directionsDisplay.setDirections({routes: []});
          polyline.setMap(null);
          polyline = null;
          for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
          }
        }
      }
    ]
  });
}

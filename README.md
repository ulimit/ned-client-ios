# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* NED client app
* Version 0.0.1

### Set up Google Maps API key ###

* Go to https://developers.google.com/maps/documentation/javascript/
* Click get a key button and continue
* Create a Project or select an existing one
* Create a unique name for your browser API key or leave it just as is then click create
* Copy the generated API key
* Open the NED/www/index.html and find this line:
```
#!html

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=REPLACE_ME_WITH_YOUR_API_KEY&libraries=places"></script>
```
* Replace the "REPLACE_ME_WITH_YOUR_API_KEY" with the one you copied
* Compile and run.

### Set up Api.ai Access token ###

* Go to https://console.api.ai/api-client/#/newAgent
* Fill out the necessary details and save 
* Click on the settings button just below the API.AI header
* Copy the right Access token (Developer for dev only)
* Open NED/www/js/speech.js and find this line:
```
#!javascript

clientAccessToken: "REPLACE_ME_WITH_YOUR_API_KEY", // insert your client access key here
```
* Replace the "REPLACE_ME_WITH_YOUR_API_KEY" with the one you copied
* Compile and run.